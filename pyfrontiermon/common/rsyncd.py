"""
"""
import datetime
import os

from pyfrontiermon.utils.net import get_addresses
from pyfrontiermon.utils.config import configuration
from pyfrontiermon.utils.logger import logger
from pyfrontiermon.utils.user import whoami


configuration.add_argument('--rsyncd-data-dir',
                           dest='rsyncd_data_dir',
                           default='/usr/lib',
                           help='Directory to store rsyncd sites data.')

configuration.add_argument('--rsyncd-user',
                           dest='rsyncd_user',
                           default=whoami()['user'],
                           help='User to run rsyncd as.')

configuration.add_argument('--rsyncd-group',
                           dest='rsyncd_group',
                           default=whoami()['group'],
                           help='Group to run rsyncd as.')

configuration.add_argument('--wpad-config-dir',
                           dest='wpad_config_dir',
                           default=None,
                           help='WPAD configuration directory.')

# TODO: Move templates in separate files
SITE_TEMPLATE = """[awstats-{site}]
    path = {data_dir}
    auth users = {site}
    hosts allow = {host_addresses}

"""

HEADER_TEMPLATE = """# (Manual changes will be lost in the next run)
# Last changed: {date}

uid = {user}
gid = {group}
list = false
use chroot = no
read only = false
write only = true
secrets file = /etc/rsyncd.secrets
"""

WPAD_CONFIG_TEMPLATE = """
[wpad-conf]
    path = {wpad_config_dir}
    read only = true
    write only = false
    hosts allow = vocms*.cern.ch cmssrv*.fnal.gov

"""


def get_site_ips(nodes):
    """ Enumerates the IPs for all DNS aliases
    """
    ips = []
    for node in nodes:
        addrs = get_addresses(node['dns_alias'])
        ips.extend(addrs)
        logger.debug("Node: {0} IPs: {1}".format(node['dns_alias'], addrs))
    return ips


def generate_rsyncd_conf(node_map):
    # Generate configuration file header
    # TODO: make cconfigurtation arguments for other global options
    now = datetime.datetime.utcnow().\
        replace(microsecond=0).isoformat(" ") + ' UTC'
    rsyncd_conf = HEADER_TEMPLATE.format(date=now,
                                         user=configuration.rsyncd_user,
                                         group=configuration.rsyncd_group )
    if configuration.wpad_config_dir:
        rsyncd_conf += WPAD_CONFIG_TEMPLATE.format(wpad_config_dir=configuration.wpad_config_dir)
        

    for (site, nodes) in sorted(node_map.get_sites().items()):
        ips = " ".join(sorted(get_site_ips(nodes)))
        if not ips:
            logger.warning("No IP addresses found for site {0}.".
                           format(site) +
                           ' Not going to include this site in configuration.')
            continue
        logger.info("Site: {0} IPs: {1}".format(site, ips))
        data_dir = os.path.expanduser(
            os.path.join(configuration.rsyncd_data_dir, site))
        # Generate site configuration
        rsyncd_conf += SITE_TEMPLATE.format(data_dir=data_dir,
                                            site=site,
                                            host_addresses=ips)
    return rsyncd_conf


if __name__ == '__main__':
    from pyfrontiermon.utils.logger import setup_logging
    from pyfrontiermon.common.nodes_map import NodesMap

    configuration.parse()
    setup_logging()
    mapper = NodesMap().load()
    conf = generate_rsyncd_conf(mapper)
    print(conf)
