""" Frontier Monitoring nodes map
"""
from collections import defaultdict
import re
try:
    from urllib2 import urlopen
except ImportError as _:
    from urllib.request import urlopen

from pyfrontiermon.utils.config import configuration


configuration.add_argument('--nodes-map-url',
                           dest='nodes_map_url',
                           default=
                           'http://wlcg-squid-monitor.cern.ch/awstatsSiteProjectNodesMapping',
                           help='Nodes map url.')


def get_site_vo(site_name):
    VOs = ['cms','atlas','cvmfs','ext']
    for vo in VOs:
        if vo not in site_name:
            continue
        return vo


class NodesMap(object):
    columns = ['site', 'dns_alias', 'awstats_name', 'role', 'mode', 'awstats_group']
    def __init__(self, src=None, remove_project_from_sitename=False):
        self.src = src or configuration.nodes_map_url
        self.remove_project_from_sitename = remove_project_from_sitename
        self.data = []

    def _load_from_url(self):
        return urlopen(self.src)

    def _load_from_file(self):
        return open(self.src, 'r')
    
    def _extract_aliases(self):
        re_pattern = re.compile(r'([\w\-]+)\(((?:[\w\-]+)(?:,\s*[\w-]+)*)\)',
                                re.IGNORECASE | re.VERBOSE | re.DOTALL)
        for entry in self.data:
            name = entry['awstats_name']
            match = re_pattern.match(name)
            # Check if name contains comma separated list of aliases
            if match:
                entry['awstats_name'], entry['awstats_aliases'] = match.groups()
                # Split comma separated list of aliases
                entry['awstats_aliases'] = [alias.strip()
                                            for alias in entry['awstats_aliases'].split(',')]
    
    def _extract_vo(self):
        for entry in self.data:
            vo = get_site_vo(entry['site'])
            entry['vo'] = vo.upper()
            if self.remove_project_from_sitename:
                entry['site'] = entry['site'].replace(vo, '')
            

    def _load_data(self, io_handle):
        if io_handle:
            # Filter commented lines and remove whitespaces
            raw_data_gen = (row.strip()
                            for row in io_handle
                            if row[0] != '#')
            self.data = [(dict(zip(self.columns, row.split())))
                         for row in raw_data_gen
                         if len(row) # ignore empty lines
                         ]
            self._extract_aliases()
            self._extract_vo()
        else:
            raise IOError('Cannot load nodes map from {0}'.format(self.src))

    def load(self):
        io_src = None
        # Download data if URL is specified, otherwise open file
        if self.src.startswith('http://'):
            io_src = self._load_from_url()
        else:
            io_src = self._load_from_file()
        self._load_data(io_src)
        return self

    def get_sites(self):
        result = defaultdict(list)
        for row in self.data:
            result[row['site']].append(row)
        return dict(result)

    def get_awstats(self):
        result = defaultdict(list)
        for row in self.data:
            site = dict(name=row['awstats_name'],
                        kind=row['mode'],
                        vo=row['vo'],
                        site=row['site'])
            if 'awstats_aliases' in row:
                site['aliases'] = row['awstats_aliases']
            # Extract group and combine by group
            if 'awstats_group' in row:
                site['group'] = row['awstats_group']
                result[row['awstats_group']].append(site)
            else:
                result[row['awstats_name']].append(site)
        # Remove duplicated entries and sort
        seen = set()
        seen_add = seen.add
        # Dict comprehension doesn't work in Py2.6
        for group, sites in result.items():
            result[group] = []
            for site in sites:
                if site['name'] not in seen:
                    seen_add(site['name'])
                    result[group].append(site)     
        return result


def replace_tab(s, tabstop=4):
    result = str()
    for c in s:
        if c == '\t':
            while (len(result) % tabstop != 0):
                result += ' ';
    else:
        result += c
    result = s.replace('\t', ' ' * tabstop ) 
    return result


if __name__ == '__main__':
    import json
    configuration.parse()

#    setup_logging()
    mapper = NodesMap(src='../../data/awstatsSiteProjectNodesMapping_tabulated', remove_project_from_sitename=True).load()
    
    print(mapper.get_awstats())
    for site in mapper.data:
#         if 'awstats_alias' in row:
        #r = json.dumps(row, indent=4, sort_keys=True)
        print("Site: {0}".format(site))
    print(json.dumps(mapper.data, indent=4))
