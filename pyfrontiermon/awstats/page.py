""" Awstats functions
"""
from collections import defaultdict
import os.path

from pyfrontiermon.utils.config import configuration
from pyfrontiermon.utils.logger import logger, setup_logging
from pyfrontiermon.utils.templates import templates_env
from pyfrontiermon.common.nodes_map import NodesMap



configuration.add_argument('--awstats-html-template',
                           dest='awstats_html_template',
                           default='awstats/www/awstats.html',
                           help='Awstats VO main HTML page template location (relative to the templates folder).')

configuration.add_argument('--awstats-html-dir',
                           dest='awstats_html_dir',
                           default='/tmp',
                           help='Awstats HTML output dir')

def get_site_vo(site_name):
    VOs = ['cms','atlas','cvmfs']
    for vo in VOs:
        if vo not in site_name:
            continue
        return vo

def get_www_template(location=None):
    location = location or configuration.awstats_html_template
    return templates_env.get_template(location)

def generate_vo_page(vo_name, vo_sites, output_dir=None):
    template = get_www_template()
    output_dir = output_dir or configuration.awstats_html_dir
    html_file_name = os.path.join(output_dir, "{0}.html".format(vo_name.lower()))
    logger.info("Going to (re)create file {0}".format(html_file_name))
    with open(html_file_name,'w') as html_file:
            vo_sites = sorted(vo_sites,
                              key=lambda a : a['group']
                                  if 'group' in a else a['sites'][0]['name'])
            html_file.write(template.render(vo_name=vo_name,
                                            sites=vo_sites))

if __name__ == '__main__':
    configuration.parse()
    setup_logging()
    nodes_map = NodesMap(src='../../data/awstatsSiteProjectNodesMapping_tabulated').load()
    entries = defaultdict(list)
    for group, sites in nodes_map.get_awstats().items():
        vo = sites[0]['vo']
        site = dict(sites=sites)
        if len(sites) > 1:
            site['group'] = group
        entries[vo].append(site)
#         site = dict(name=name,
#                     sites=[ dict(zip(['kind','name'],tuplezied))
#                             for tuplezied in set(
#                                                  [(entry['mode'],entry['awstats_name'])
#                                                   for entry in site_data])])
#         #for entry in site_data:
#         #    site['sites'].append(dict(kind=entry['mode'],
# #                                       name=entry['awstats_name']))
# #             print(entry)
#         sites[vo].append(site)
#     template = templates_env.get_template(configuration.awstats_html_template)
    print(entries)
    for vo_name, vo_sites in entries.items():
        generate_vo_page(vo_name, vo_sites)
#         print(vo_name, vo_sites)
#         with open("/tmp/{0}.html".format(vo_name.lower()),'w') as html_file:
#             vo_sites = sorted( vo_sites, key = lambda a : a['group'] if 'group' in a else a['sites'][0]['name'] )
#             html_file.write(template.render(vo_name=vo_name, sites=vo_sites))
            #print(template.render(vo_name=vo_name, sites=vo_sites))
    
#         print(name, get_site_vo(name), site)
#         site = dict(name=name, kind=site_data[])
#         sites[ get_site_vo(name).upper ].append()
    #print( template.render(vo_name='CMS', sites=[{'kind':'test','name':'test'}]))
