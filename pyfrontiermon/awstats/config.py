''' AWstats configuration routines
'''
import re

import jinja2

from pyfrontiermon.utils.config import configuration
from pyfrontiermon.utils.logger import logger, setup_logging
from pyfrontiermon.utils.templates import templates_env
# awstats_config_template: /home/dbfrontier/local/apache/awstats/wwwroot/cgi-bin/awstats.model.conf

configuration.add_argument('--awstats_config_template',
                           dest='awstats_config_template',
                           default=
                           '/home/dbfrontier/local/apache/awstats/wwwroot/cgi-bin/awstats.model.conf',
                           help='AWStats configuration file template.')

class AWStatsConfigruation(object):
    def __init__(self, nodes_map):
        self.nodes_map = nodes_map
    
    
def get_config_template(location=None):
    def change_tags(match):
        tag = match.group(1).lower()
        return "{{{{ {0} }}}}".format(tag)

    regex = r"@@@([\w]+)@@@"
    with open(configuration.awstats_config_template, 'r') as tmpl_file:
        tmpl = [line.strip()
                for line in tmpl_file 
                if len(line.strip()) and
                   (line.strip()[0] != '#') and 
                   (line.strip() != '')]
    tmpl = [re.sub(regex, change_tags, line)
            for line in tmpl]
    with open('../templates/awstats/config/awstats.model.conf','w') as tmpl_file:
        tmpl_file.write("\n".join(tmpl))
    template = jinja2.Environment().from_string("\n".join(tmpl))
    return template


if __name__ == '__main__':
    from pyfrontiermon.common.nodes_map import NodesMap
    args = ['--awstats_config_template',
            '../../data/awstats.model.conf',
            '--nodes-map-url',
            '../../data/awstatsSiteProjectNodesMapping_new']
    configuration.parse(args)
    setup_logging()

    print(configuration.nodes_map_url)
    print(configuration.awstats_config_template)

    nodes_map = NodesMap(remove_project_from_sitename=True).load()
    template = get_config_template()
    for group, sites in nodes_map.get_awstats().items():
        print(group)
        for entry in sites:
            print(entry)
            name = entry['alias'] if 'alias' in entry else entry['name']
            config_data = dict(sitename=entry['site'],
                               experiment=entry['vo'].lower(),
                               machinealias=name)
            entry_config = template.render(**config_data)
            if 'alias' in entry:
                print(entry_config)
    #aws_conf = AWStatsConfigruation(nodes_map=nodes_map)
    