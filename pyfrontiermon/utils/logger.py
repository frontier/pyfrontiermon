import logging
import sys

from pyfrontiermon.utils.config import configuration

py_version = sys.version_info[0:2]

_levelToName = {
    logging.CRITICAL: 'CRITICAL',
    logging.ERROR: 'ERROR',
    logging.WARNING: 'WARNING',
    logging.INFO: 'INFO',
    logging.DEBUG: 'DEBUG',
    logging.NOTSET: 'NOTSET',
}

_nameToLevel = dict([(v, k)
                     for k, v in _levelToName.items()])


def check_log_level(level):
    result = None
    if isinstance(level, int): # Level is int
        result = level
    elif str(level) == level: # Level is string
        if level not in _nameToLevel:
            raise ValueError("Unknown level: {0}".format(level))
        result = _nameToLevel[level]
    else:
        raise TypeError("Level not an integer or a valid string: {0}".format(level))
    return result


configuration.add_argument('--log-file',
                           dest = 'log_file',
                           default = None,  # '/var/log/frontiermon.log',
                           help = 'Log file')

configuration.add_argument('--log-level',
                           dest = 'log_level',
                           default = 'INFO',
                           type = check_log_level,
                           nargs = '?',
                           help = 'Log level. '
                           'Choose one of {0}'.format(list(_nameToLevel.keys()))
                           )


logger = logging.getLogger('FrontierMon')

def get_logger():
    return logger


def setup_logging(logger=logger):
    # logger = logging.getLogger(name)
    logger.setLevel(configuration.log_level)
    ch = logging.StreamHandler()
    if configuration.log_file :
        ch = logging.FileHandler(configuration.log_file)

    ch.setLevel(configuration.log_level)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)
    return logger
