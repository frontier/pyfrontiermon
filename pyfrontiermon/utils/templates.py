''' Templates module
'''
import os

from jinja2 import Environment, PackageLoader

from pyfrontiermon.utils.config import configuration

# # Expects template folder in the main directory of the package
# __path = os.path.realpath( # Normalize path
#                 os.path.join( # Join paths 
#                              os.path.dirname(__file__), # Get this module's dir
#                              '../templates' ) )
# 
# configuration.add_argument('--templates-dir',
#                            dest = 'templates_dir',
#                            default = __path,  # '/var/log/frontiermon.log',
#                            help = 'Template base dir.')

# Setup Jinja templates environment 
templates_env = Environment(loader=PackageLoader('pyfrontiermon', 'templates'))