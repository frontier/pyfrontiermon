""" User management
"""
import getpass
import grp
import pwd


def whoami():
    """ Returns the user and group names as well as uid and gid 
    of the current user.
    """
    user = getpass.getuser()
    uid = pwd.getpwnam(user).pw_uid
    gid = pwd.getpwnam(user).pw_gid
    group = grp.getgrgid(gid)[0]
    return dict(user=user,
                group=group,
                uid=uid,
                gid=gid)


if __name__ == '__main__':
    info = whoami()
    print(info)
