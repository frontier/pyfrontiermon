""" Network routines
"""
import re

from pyfrontiermon.utils.logger import logger as log


def is_valid_ipv4(ip_address):
    """ Checks if a string `ip_address` represents a valid IPv4 address.
    """
    octet_patern = r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
    ip_pattern = r"^{0}\.{0}\.{0}\.{0}$".format(octet_patern)
    return re.match(ip_pattern, ip_address)


def get_addresses(hostname):
    """ Returns all IPv4 & IPv6 addresses related to an alias.
    """
    import socket
    if is_valid_ipv4(hostname):
        return [hostname]
    result = []
    try:
	# default family is 0 which is both IPv4 and IPv6
        info = socket.getaddrinfo(hostname, 0)
        result = list(set(e[4][0] for e in info))
    except (socket.error, socket.herror):
        log.warning('Cannot enumerate addresses for {0}'.
                       format(hostname))
    return result


if __name__ == '__main__':
    result = get_addresses('lxplus.cern.ch')
    print(result)
