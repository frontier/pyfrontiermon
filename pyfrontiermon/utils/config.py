import argparse
import sys


class LoadFromFileAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        contents = None
        with values as config_file:
            # Load values to memory
            contents = config_file.read()
        data = parser.parse_args(contents.split())
        for key, value in data.items():
            if value and (key != option_string.lstrip('-')):
                setattr(namespace, key, value)


class Configuration(object):
    def __init__(self):
        self.__parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
        self.__parser.add_argument('--config',
                                   '-c',
                                   type=open,
                                   action=LoadFromFileAction)

    def add_argument(self, *args, **kwargs):
        """ Wrapper around ArgumentParser.add_argument.
        """
        self.__parser.add_argument(*args, **kwargs)

    def parse(self, arguments=sys.argv[1:]):
        self.__parser.parse_args(arguments, namespace=self)#namespace=configuration)
        #self.__dict__.update(vars(args))


configuration = Configuration()


if __name__ == '__main__':
    configuration.add_argument( '--foo', default=0)
    configuration.parse()
    print(configuration.foo)
