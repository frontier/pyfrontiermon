# Frontier Monitoring Python modules.

## Structure

* common - contains common structures and methods used by launchpad and wlc-squid-mon configurations
* utils - general functions and structures

## Installation

``` bash
python setup.py build
sudo python setup.py install
```

Do the install on all 4 frontiermon and wlcgsquidmon machines.

## Test

``` bash
python -m unittest discover -v tests
```
