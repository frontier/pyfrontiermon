#!/usr/bin/env python
# Always prefer setuptools over distutils
from codecs import open
import os

from setuptools import setup, find_packages
import sys

if sys.version_info[:2] < (2, 6) or (3, 0) <= sys.version_info[0:2] < (3, 2):
    raise RuntimeError("Python version 2.6 or >= 3.2 required.")

here = os.path.abspath(os.path.dirname(__file__))

# Fetch long description from the readme file
with open(os.path.join( here, 'README.md'), 'r', encoding='utf-8') \
     as readme_file:
    long_description = readme_file.read()

print(find_packages(exclude=['tests',
                             'tests.common',
                             'tests.utils']))

setup(
    name='pyfrontiermon',
    version='0.1.3-2',
    description='Frontier Monitoring Python modules',
    long_description=long_description,
    author='Vassil Verguilov',
    author_email='vassil.verguilov@gmail.com',
    url='https://gitlab.cern.ch/frontier/pyfrontiermon',
    scripts=['bin/generate_rsyncd_config'],
    classifiers=['Development Status :: 3 - Alpha',
                 'Intended Audience :: DevOps',
                 'Topic :: System Administration :: Monitoring and Configuration Tools',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.6',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.2',
                 'Programming Language :: Python :: 3.3',
                 'Programming Language :: Python :: 3.4',
                 'Programming Language :: Python :: 3.5',
             ],
    keywords='frontier monitoring tools',
    #install_requires=[''],
    #extras_require={
    #    'dev': ['check-manifest'],
    #    'test': ['coverage']
    #},
    include_package_data=True,
    zip_safe=False,
    install_requires=['Jinja2','argparse'],
    #setup_requires=['pytest-runner'],
    #tests_require=['pytest'],
#    package_data={'pyfrontiermon': ['templates/*']},
    test_suite="tests",
    packages=find_packages(exclude=['tests',
                                    'tests.common',
                                    'tests.utils'])
#    ['pyfrontiermon',
#              'pyfrontiermon.utils',
#              'pyfrontiermon.common'])
)
