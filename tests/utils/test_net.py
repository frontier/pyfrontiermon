import unittest
from pyfrontiermon.utils import net


class TestNetModule(unittest.TestCase):
    def setUp(self):
        pass

    def test_is_valid_ipv4(self):
        self.assertTrue(net.is_valid_ipv4('127.0.0.1'))
        self.assertTrue(net.is_valid_ipv4('10.10.10.1'))
        self.assertFalse(net.is_valid_ipv4('127.0.0.1000'))
        self.assertFalse(net.is_valid_ipv4('325.0.0.1'))


if __name__ == '__main__':
    unittest.main()
