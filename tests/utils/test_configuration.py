'''
Created on Jul 19, 2016

@author: vasoto
'''
import unittest

from pyfrontiermon.utils.config import Configuration


class TestConfiguration(unittest.TestCase):


    def setUp(self):
        self.config = Configuration() 

    def tearDown(self):
        pass


    def testAddArgument(self):
        self.config.add_argument( '--foo', default=42)
        self.config.parse([])
        self.assertEqual(self.config.foo, 42, "add_argument failed.")
    
    def testParse(self):
        self.config.add_argument('--foo', default=0, type=int)
        self.config.add_argument('--bar', default=5, type=int)
        self.config.add_argument('--gar', default="abcd", type=str)
        
        self.config.parse(arguments=['--foo', '42', '--bar', '10', '--gar', '100'])
        self.assertEqual(self.config.foo, 42, "parse failed.")
        self.assertEqual(self.config.bar, 10, "parse failed.")
        self.assertEqual(self.config.gar, '100', "parse failed.")
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()