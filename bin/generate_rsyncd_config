#!/bin/env python
import logging

from pyfrontiermon.utils.config import configuration
from pyfrontiermon.utils.logger import logger, setup_logging
from pyfrontiermon.common.nodes_map import NodesMap
from pyfrontiermon.common.rsyncd import generate_rsyncd_conf

configuration.add_argument('--rsyncd-conf-file',
                           '-f',
                           dest='rsyncd_conf_file',
                           # default='/etc/rsyncd.conf',
                           help='Location of the rsyncd configuration file.')


def save_to_file( file_name, data ):
    with open(file_name, 'w') as out_file:
        out_file.write(data)


def main():
    # Parse command line configuration
    configuration.parse()
    # Setup logging
    setup_logging()
    # Load nodes map
    logger.info('### Begin rsyncd configuration generation')
    mapper = NodesMap().load()
    # Generate rsyncd configuration
    rsyncd_conf = generate_rsyncd_conf(mapper)
    if configuration.rsyncd_conf_file :
        save_to_file(configuration.rsyncd_conf_file,
                     rsyncd_conf)
    else:
        print(rsyncd_conf)
    logger.info('### Stop rsyncd configuration generation')


if __name__ == '__main__':
    main()
